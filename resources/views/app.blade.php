<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="/css/app.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.14.3/dist/css/uikit.min.css" />
    </head>

    <body>
    <div class="uk-background-secondary uk-light">
    <div class="uk-container">
    <nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
    <div class="uk-navbar-left">

        <ul class="uk-navbar-nav">
            <!--<li>
                <a href="#">Felhasználók</a>
                <div class="uk-navbar-dropdown">
                    <ul class="uk-nav uk-navbar-dropdown-nav">
                    </ul>
                </div>
            </li>-->
            <li class=""><a href="{{ route('users.index') }}">Felhasználók</a></li>
            <!--<li>
                <a href="#">Járművek</a>
                <div class="uk-navbar-dropdown">
                    <ul class="uk-nav uk-navbar-dropdown-nav">

                    </ul>
                </div>
            </li>-->
            <li class=""><a href="{{ route('vehicles.index') }}">Járművek</a></li>
            <li><a href="#">Item</a></li>
        </ul>

        </div>
    </nav>
    </div>
</div>
    <div id="app" class="uk-container">
        <div class="uk-card-body">
            @yield('content')
        </div>
    </div>
        <script src="https://cdn.jsdelivr.net/npm/uikit@3.14.3/dist/js/uikit.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/uikit@3.14.3/dist/js/uikit-icons.min.js"></script>
        <script src="/js/app.js"></script>
    </body>
</html>
