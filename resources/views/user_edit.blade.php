@extends('app')
@section('content')
<form class="uk-form-stacked" action="{{ url('/users/'.$id) }}" method="POST">
    @csrf
    @method('PUT')
    <label for="last_name">Vezetéknév
        <input class="uk-input" value="{{$user->last_name}}" name="last_name" id="last_name" />
    </label>
    <label for="first_name">Keresztnév
        <input class="uk-input" value="{{$user->first_name}}" name="first_name" id="first_name" />
    </label>
    <label for="email">E-mail
        <input class="uk-input" value="{{$user->email}}" name="email" id="email" />
    </label>
    <button class="uk-button uk-button-primary" type="submit">Küldés</button>
</form>
<h2>Hozzá tartozó járművek:</h2>
<div uk-grid class="uk-grid-small uk-child-width-1-3@m">
    @foreach($owned_vehicles as $v)
    <div class="uk-card uk-card-default">
        <div class="uk-card-body">
            {{$v->plate_number}}
        </div>
    </div>
    @endforeach
</div>
@endsection
