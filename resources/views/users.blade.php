@extends('app')
@section('content')
<div class="uk-grid-small" uk-grid>
    <div><a href="{{route('users.login')}}" class="uk-button uk-button-primary">Bejelentkezés</a></div>
    <div><a href="{{route('users.register')}}" class="uk-button uk-button-primary">Regisztráció</a></div>
</div>
<table class="uk-table uk-table-hover uk-table-striped">
    <thead>
        <tr>
            <th>Vezetéknéb</th>
            <th>Keresztnév</th>
            <th>Email</th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{$user->first_name}}</td>
            <td>{{$user->last_name}}</td>
            <td>{{$user->email}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection

