@extends('app')
@section('content')
<form class="uk-form-stacked" action="{{route('users.store')}}" method="POST">
    @csrf
    <label for="last_name">Vezetéknév
        <input class="uk-input" name="last_name" id="last_name" />
    </label>
    <label for="first_name">Keresztnév
        <input class="uk-input" name="first_name" id="first_name" />
    </label>
    <label for="email">E-mail
        <input class="uk-input" name="email" id="email" />
    </label>
    <button class="uk-button uk-button-primary" type="submit">Küldés</button>
</form>
@endsection
