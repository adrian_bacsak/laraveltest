@extends('app')
@section('content')
<form method="post" action="{{ route('postlogin') }}">
    <div class="uk-card uk-width-1-3@m uk-align-center uk-card-small uk-card-default">
        <div class="uk-card-head">
            <h3 class="uk-text-center">Bejelentkezés</h3>
        </div>
        <div class="uk-card-body">
                <div class="uk-child-width-1-1@m">
                    <div class="uk-inline">
                        <span class="uk-form-icon" uk-icon="icon: user"></span>
                        <input name="email" id="email" type="email" class="uk-input uk-input-small uk-form-small" />
                    </div>
                    <div class="uk-inline uk-margin-medium-top">
                        <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
                        <input name="password" id="password" type="password" class="uk-input uk-input-small uk-form-small" />
                    </div>
                </div>
                <div class="uk-margin-medium-top">
                    <button type="submit" class="uk-button uk-button-primary uk-width-1-1@m">Bejelentkezés</button>
                </div>
        </div>
    </div>
</form>
@endsection