@extends('app')
@section('content')
<form method="post" action="{{ route('postregister') }}">
    <div class="uk-card-body uk-card uk-card-default">
        <div uk-grid class="uk-grid-small uk-child-width-1-2@m">
            <div>
                <label for="last_name">Vezetéknév
                    <input name="last_name" id="last_name" type="text" class="uk-input" />
                </label>
            </div>
            <div>
                <label for="first_name">Keresztnév
                    <input name="first_name" id="first_name" type="text" class="uk-input" />
                </label>
            </div>
            <div class="uk-width-1-1@m">
                <label for="email">Email
                    <input name="email" id="email" type="email" class="uk-input" />
                </label>
            </div>
            <div>
                <label for="password">Jelszó
                    <input name="password" id="password" type="password" class="uk-input" />
                </label>
            </div>
            <div>
                <label for="confirm_password">Jelszó mégegyszer
                    <input name="confirm_password" id="confirm_password" type="password" class="uk-input" />
                </label>
            </div>  
        </div>
        <button class="uk-button uk-button-primary uk-margin-small-top uk-width-1-1@m" type="submit">Küldés</button>
    </div>
</form>
@endsection