@extends('app')
@section('content')
<h1>Vehicle View</h1>
<table class="uk-table uk-table-striped uk-table-responsive uk-table-small uk-table-hover">
        <caption class="uk-text-lead">Járművek listája</caption>
        <thead>
            <tr>
                <th>Rendszám</th>
                <th>Tulajdonos</th>
                <th>Szerkesztés</th>
            </tr>
        </thead>
        <tbody>
            @foreach($vehicles as $v)
                <tr>
                    <td>{{$v->plate_number}}</td>
                    <td>{{$v->owner_id}}</td>
                    <td><a class="uk-button uk-button-primary uk-button-small" href="{{ route('vehicles.edit', ['vehicle' => $v->id]) }}">Szerkesztés</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{$vehicles}}
@endsection