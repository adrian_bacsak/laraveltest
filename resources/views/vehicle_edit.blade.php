@extends('app')
@section('content')
<form class="uk-form-stacked" action="{{ route('vehicles.update', ['vehicle' => $vehicle->id]) }}" method="post">
    @csrf
    @method('PUT')
    <label for="plate_number">Rendszám:
        <input class="uk-input" name="plate_number" id="plate_number" value="{{$vehicle->plate_number}}" />
    </label>
    <label for="owner">Tulajdonos
        <select name="owner_id" id="owner" class="uk-select" value="{{$vehicle->owner_id}}">
                <option disabled>Jelenlegi tulajdonos: {{$vehicle->owner_id}} sorszámú személy</option>
            @foreach($users as $user)
                <option name="owner_id" value="{{$user->id}}">{{'Sorszám: '.$user->id.' Teljesnév:'.$user->last_name.' '.$user->first_name}}</option>
            @endforeach
        </select>
    </label>
    <button class="uk-button uk-button-primary" type="submit">Küldés</button>
</form>
<h2>Jelenlegi tulajdonos:</h2>
<div class="uk-text-muted">{{$owner->last_name.' '. $owner->first_name}}</div>
@endsection
