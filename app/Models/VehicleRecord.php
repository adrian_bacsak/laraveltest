<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleRecord extends Model
{
    use HasFactory;

    protected $table = 'vehicles';

    protected $primaryKey = 'id';

    public $incrementing = true;

    public function owner() {
        return $this->belongsTo(UserRecord::class, 'owner_id');
    }
}
