<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticable;


class UserRecord extends Authenticable
{
    use HasFactory, Notifiable;

    protected $table = 'users';

    protected $primaryKey = 'id';

    public $incrementing = true;

    public function vehicles() {
        return $this->hasMany(VehicleRecord::class, 'owner_id');
    }

    public function scopeuserExists($query, $email) {
        return $query->where('email', '=', $email);
    }
}
