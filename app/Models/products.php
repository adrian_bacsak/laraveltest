<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class products extends Model
{
    use HasFactory;

    public function scopeInStock($query) {
        return $query->where('stock_qty', '>', '0');
    }

    public function scopeFilter($query, $array) {
        $query->where('products.title', 'like', '%'.$array['title'].'%');
        $query->where('manufacturers.name', 'like', '%'.$array['manufacturer'].'%');
        $query->where('brands.title', 'like', '%'.$array['brand'].'%');
        if($array['base_price_eur_min'] ?? false)
        $query->where('products.base_price_eur', '>', $array['base_price_eur_min']);
        if($array['base_price_eur_max'] ?? false)
            $query->where('products.base_price_eur', '<', $array['base_price_eur_max']);
    }
}
