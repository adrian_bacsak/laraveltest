<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserRecord;
use App\Http\Requests\UserRequest;
use App\Models\VehicleRecord;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Models\Products;
use App\Models\exchange_rates;
use Illuminate\Support\Facades\Hash;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function login() {
        return view('login');
    }
    public function postlogin(Request $request) {
        $user = UserRecord::userExists($request->email)->first();

        if($user && Hash::check($request->password, $user->password)) {
            Auth::login($user);
            return redirect()->route('users.index');
        } else {
            echo "Nincs ilyen felhasználó!";
        }
    }
    public function register() {
        return view('register');
    }
    public function postregister(Request $request) {
        $user = UserRecord::userExists(request('email'))->first();
        $password = Hash::make($request->password);
        if($user) {
            echo 'van ilyen felhasználó';
        } else {
            if (Hash::check($request->password, $password) && Hash::check($request->confirm_password, $password)) {
                $request->password = $password;
                $request->confirm_passowrd = $password;
                $this->store($request);
            } else {
                echo 'jelszavak nem egyeznek';
            }
        }
    }
    public function index()
    {
        $users = Cache::remember('users', now()->addSeconds(15), function () {
            return UserRecord::paginate(20);
        });

        return view('users',
        ['users' => $users]);

        /*$Products =
            Products::InStock()
            ->leftJoin("manufacturers", "manufacturers.id", "=", "manufacturer_id")
            ->leftJoin("brands", "brands.id", "=", "brand_id")
            ->get('base_price_eur');

        $exchange_rate =
            exchange_rates::orderBy('id', 'DESC')->first('eurhuf');

        foreach($Products as $p) {
            echo($p->base_price_eur * $exchange_rate->eurhuf);
            echo('<br>');
        }*/

        /*$products = Products::leftJoin('manufacturers', 'manufacturers.id', '=', 'manufacturer_id')
            ->leftJoin("brands", "brands.id", "=", "brand_id")
            ->Filter(['title' => request('title'),
                    'base_price_eur_min' => request('base_price_eur_min'),
                    'base_price_eur_max' => request('base_price_eur_max'),
                    'manufacturer' => request('manufacturer_name'),
                    'brand' => request('brand_name')])
            ->select('products.title as ProductTitle', 'products.base_price_eur as ProductPrice', 'manufacturers.name as ManufacturerName', 'brands.title as BrandTitle')
            ->get(['products.title', 'products.base_price_eur', 'manufacturers.name', 'brands.title']);
        return view('users',
            ['products' => $products]
        );*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users_create',
        ['vehicles' => VehicleRecord::all()
    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new UserRecord;

        $user->email = $request->email;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->password = $request->password;
        
        $user->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return UserRecord::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('user_edit',
            ["id" => $id,
            "user" => UserRecord::find($id),
            "vehicles" => VehicleRecord::all(),
            "owned_vehicles" => UserRecord::find($id)->vehicles
            ]
    );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $user = UserRecord::find($id);

        $request->validated();

        $user->email = $request->email;
        $user->last_name = $request->last_name;
        $user->first_name = $request->first_name;
        $user->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = UserRecord::find($id);

        $user->delete();
    }
}
