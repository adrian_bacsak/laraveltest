<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
class UserController extends Controller
{
    public function GetAllUser() {
        $users = User::paginate(2);

        return $users;
    }

    public function GetAllUserView() {
        $users = $this->GetAllUser();

        return view('users', ['users'=>$users]);
    }

    public function GetUserById($int) {
        $user = User::find($int);

        return $user;
    }

    public function PostUser(Request $request) {
        $user = new User;

        $user->email = $request->email;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;

        $user->save();

        return 'Az '.$user->email.' Hozzáadása sikeresen megtörtént!';
    }

    public function PutUser(Request $request, int $id) {
        $user = User::find($id);

        if($user) {
            $user->email = $request->email;
            $user->last_name = $request->last_name;
            $user->first_name = $request->first_name;

            $user->save();
        } else {
            return "Sikertelen!";
        }
    }

    public function DeleteUser(Request $request, int $id) {
        $user = User::find($id);

        if($user) {
            $user->delete();
            return "A törlés sikeres volt!";
        } else {
            return "A törlés sikertelen volt!";
        }
    }
}
