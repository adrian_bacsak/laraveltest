<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    private array $array1 = array();
    private array $array2 = array();

    public function teszt() {
        for($i=0; $i<20; $i++) {
            array_push($this->array1, rand(1,100));
            array_push($this->array2, rand(1,100));
        }
        /*echo(count($this->metszet()).'darab');
        echo('<div> <table> <tr>');
        for($i=0; $i<count($this->metszet()); $i++) {
            echo('<th>'.$i.'</th>');
        }
        echo('</tr><tr>');
        foreach($this->metszet() as $s) {
            echo('<td>'.$s.'</td>');
        }
        echo('</tr>');
        echo('</table></div>');*/

        return view('teszt', ['count' => count($this->metszet()), 'metszet' => $this->metszet()]);

	}

    private function metszet() : array {
        return array_intersect($this->array1, $this->array2);
    }
}
