<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VehicleRecord;
use App\Http\Requests\VehiclePostRequest;
use App\Models\UserRecord;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('vehicles', [
            'vehicles' => VehicleRecord::paginate(20)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePostRequest  $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('vehicle_edit', [
            //'plate_number' => VehicleRecord::find($id)->plate_number,
            //'id' => $id,
            'vehicle' => VehicleRecord::find($id),
            'users' => UserRecord::all(),
            'owner' => VehicleRecord::find($id)->owner
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StorePostRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VehiclePostRequest $request, $id)
    {
        $vehicle = VehicleRecord::find($id);

        $vehicle->plate_number = $request['plate_number'];
        $vehicle->owner_id = $request['owner_id'];
        $vehicle->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
