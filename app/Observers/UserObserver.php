<?php

namespace App\Observers;

use App\Models\UserRecord;
use App\Jobs\UserChangeJob;

class UserObserver
{
    /**
     * Handle the UserRecord "created" event.
     *
     * @param  \App\Models\UserRecord  $userRecord
     * @return void
     */
    public function created(UserRecord $userRecord)
    {
        //
    }

    /**
     * Handle the UserRecord "updated" event.
     *
     * @param  \App\Models\UserRecord  $userRecord
     * @return void
     */
    public function updated(UserRecord $userRecord)
    {
        dispatch(new UserChangeJob($userRecord));
    }

    /**
     * Handle the UserRecord "deleted" event.
     *
     * @param  \App\Models\UserRecord  $userRecord
     * @return void
     */
    public function deleted(UserRecord $userRecord)
    {
        //
    }

    /**
     * Handle the UserRecord "restored" event.
     *
     * @param  \App\Models\UserRecord  $userRecord
     * @return void
     */
    public function restored(UserRecord $userRecord)
    {
        //
    }

    /**
     * Handle the UserRecord "force deleted" event.
     *
     * @param  \App\Models\UserRecord  $userRecord
     * @return void
     */
    public function forceDeleted(UserRecord $userRecord)
    {
        //
    }
}
