<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class WriteToFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'write:file {number}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Write random numbers to a file.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $number = $this->argument('number');
        $array = array();
        for($i=0; $i<$number; $i++) {
            array_push($array, rand(1,100));
        }
        file_put_contents("tesztfile.txt", json_encode($array));
    }
}
