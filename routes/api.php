<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VehicleController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('users/login', [UserController::class, 'postlogin'])->name('postlogin');
Route::post('users/register', [UserController::class, 'postregister'])->name('postregister');
//Route::post('products/filter', [UserController::class, 'Filter']);
/*Route::get("users", [UserController::class, 'GetAllUser']);
Route::get("users/{id}", [UserController::class, 'GetUserById']);
Route::post("users", [UserController::class, 'PostUser']);
Route::delete("users/{id}", [UserController::class, 'DeleteUser']);
Route::put("users/{id}", [UserController::class, "PutUser"]);*/
