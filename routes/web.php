<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\VehicleController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return view ('app');
});

Route::get("teszt", [TestController::class, 'teszt']);
Route::resource('users', UserController::class);
Route::get("login", [UserController::class, 'login'])->name('users.login');
Route::get("register", [UserController::class, 'register'])->name('users.register');
Route::resource('vehicles', VehicleController::class);