<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\UserRecord;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*DB::table('users')->insert([
            'email' => Str::random(10),
            'last_name' => Str::random(10),
            'first_name' => Str::random(10),
        ]);*/

        UserRecord::factory()
            ->count(50)
            ->create();
    }
}
